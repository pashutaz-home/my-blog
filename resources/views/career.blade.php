@extends('layouts.main')

@section('content')
    <div id="wrapper2">
        <div id="featured" class="container">
            <div class="box1">
                <h2><span class="icon icon-group"></span>HSR24</h2>
                <p>Aliquam erat volutpat. Pellentesque tristique ante ut risus. Quisque dictum. Integer nisl risus, sagittis convallis, rutrum id, elementum congue, nibh. Suspendisse dictum porta lectus. Donec placerat odio vel elit. Nullam ante orci, pellentesque eget, tempus quis, ultrices in, est. Curabitur sit amet nulla. Donec leo, vivamus fermentum nibh in augue praesent a lacus at urna congue rutrum.</p>
            </div>
            <div class="box2">
                <h2><span class="icon icon-briefcase"></span>DTI algorithmic</h2>
                <p>Aliquam erat volutpat. Pellentesque tristique ante ut risus. Quisque dictum. Integer nisl risus, sagittis convallis, rutrum id, elementum congue, nibh. Suspendisse dictum porta lectus. Donec placerat odio vel elit. Nullam ante orci, pellentesque eget, tempus quis, ultrices in, est. Curabitur sit amet nulla. Donec leo, vivamus fermentum nibh in augue praesent a lacus at urna congue rutrum.</p>
            </div>
        </div>
    </div>
@endsection