@extends('layouts.main')


@section('content')
    <div class="wrapper">
        <div id="page" class="container">
            <h1>New Article</h1>

            <form action="/articles" method="post">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input 
                        type="text" 
                        class="form-control {{ $errors->has('title') ? 'is-invalid' : ''}}" 
                        id="title" 
                        name="title" 
                        aria-describedby="titleHelp" 
                        placeholder="Enter title"
                        value="{{ old('title')}}"
                    >
                    @if ($errors->has('title'))
                        <small id="titleHelp" class="form-text text-danger">{{ $errors->first('title') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="summary">Summary</label>
                    <textarea class="form-control" id="summary" name="summary" rows="3">{{ old('summary') }} </textarea>
                </div>
                <div class="form-group">
                    <label for="body">Body of article</label>
                    <textarea class="form-control @error('body') is-invalid @enderror}}" id="body" name="body" rows="3">{{ old('body') }}</textarea>
                    @error('body')
                        <small id="titleHelp" class="form-text text-danger">{{ $errors->first('body') }}</small>                        
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tags">Tags</label>
                    <select class="custom-select" name="tags[]" id="tags" multiple>
                        @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>

                    @error('tags')
                        <small id="tagsHelp" class="form-text text-danger">{{ $message }}</small>
                    @endif
                </div>
                <button type="submit" class="btn btn-success">Create article</button>
            </form>
        </div>
    </div>
@endsection