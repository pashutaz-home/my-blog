@extends('layouts.main')


@section('content')
	<main role="main">
		<section class="jumbotron text-center">
			<div class="container">
				<h1>Pashutaz blog</h1>
				<p class="lead text-muted">Welcome to my blog/portfolio</p>
				<p>
					<a href="#" class="btn btn-secondary my-2">Search articles</a>
					<a href="/articles/create" class="btn btn-primary my-2">Create new article</a>
				</p>
			</div>
		</section>

		<div class="album py-5 bg-light">
		<div class="container">
		<div class="row">
			@forelse ($articles as $article)
				<div class="col-md-4">
				<div class="card mb-4 shadow-sm">
					<svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>{{$article->title}}</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">{{$article->title}}</text></svg>
					<div class="card-body">
						<p class="card-text">{{ $article->summary }}</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<a href="{{ $article->path() }}"><button type="button" class="btn btn-sm btn-outline-info">View</button></a>
								<a href="{{ $article->path() }}/edit"><button type="button" class="btn btn-sm btn-outline-secondary">Edit</button></a>
							</div>
							<small class="text-muted">{{ $article->updated_at }}</small>
						</div>
					</div>
				</div>
				</div>
			@empty
				<p>No articles with that tag</p>
			@endforelse
		</div>
		</div>
		</div>
	</main>
@endsection



