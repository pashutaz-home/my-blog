@extends('layouts.main')


@section('content')
	<main role="main">
	<section class="jumbotron">
		<div class="container">
			<h1>{{ $article->title }}</h1>
			<p style="margin-top: 10px;">
				@foreach ($article->tags as $tag)
					<a href="/articles?tag={{ $tag->name }}" style="padding: 2px;border: 1px solid #007bff; border-radius: 5px">{{ $tag->name }}</a>
				@endforeach
			</p>
			<h2>{{ $article->summary }}</h2>
			<p class="lead text-muted">
				{{ $article->body }}
			</p>
			<p>
				<a href="{{ url()->current() }}/edit" class="btn btn-primary my-2">Edit article</a>
			</p>
		</div>
	</section>
	</main>
@endsection

