@extends('layouts.main')


@section('content')
    <div class="wrapper">
        <div id="page" class="container">
            <h1>Editing Article</h1>

            <form action="{{$article->path()}}" method="post" style="display: inline">
                @csrf
                @method('patch')
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" aria-describedby="titleHelp" placeholder="Enter title" value="{{ $errors->has('title') ? old('title') : $article->title }}">
                    @error('title')
                        <small id="titleHelp" class="form-text text-danger">{{ $errors->first('title') }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="summary">Summary</label>
                    <textarea class="form-control" id="summary" name="summary" rows="3">{{ $errors->has('summary') ? old('summary') : $article->summary }}</textarea>
                </div>
                <div class="form-group">
                    <label for="body">Body of article</label>
                    <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3">{{ $errors->has('body') ? old('body') : $article->body }}</textarea>
                    @error('body')
                        <small id="bodyHelp" class="form-text text-danger">{{ $errors->first('body') }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tags">Tags</label>
                    <select class="custom-select" name="tags[]" id="tags" multiple>
                        @foreach ($tags as $tag)
                            <option 
                                value="{{ $tag->id }}" 
                                @foreach ($article->tags as $activetag)
                                    {{ $activetag->id == $tag->id ? 'selected=selected' : '' }}
                                @endforeach 
                            >
                                {{ $tag->name }}
                            </option>
                        @endforeach
                    </select>

                    @error('tags')
                        <small id="tagsHelp" class="form-text text-danger">{{ $message }}</small>
                    @endif
                </div>
                <a href="{{ $article->path() }}" class="btn btn-primary">Cancel</a>
                <button type="submit" class="btn btn-success">Save edit</button>
            </form>

            <form action="{{ $article->path() }}" method="post" class="float-right form-inline" onSubmit="return confirm('Are you sure you wish to delete?');">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger my-2" style="margin: 0!important;">Delete this article</button>
            </form>
        </div>
    </div>
@endsection