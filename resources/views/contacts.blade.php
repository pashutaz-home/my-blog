@extends('layouts.main')

@section('contacts')
    <div class="title">
        <h2>Get in touch</h2>
        <span class="byline">Phasellus nec erat sit amet nibh pellentesque congue</span> 
    </div>
    <ul class="contact">
        <li><a href="https://twitter.com/pashutaz" target="_blank" class="icon icon-twitter"><span>Twitter</span></a></li>
        <li><a href="https://www.facebook.com/pashutaz" target="_blank" class="icon icon-facebook"><span>Facebook</span></a></li>
        <li><a href="https://github.com/pashutaz" target="_blank" class="icon icon-rss"><span>Github</span></a></li>
    </ul>
@endsection