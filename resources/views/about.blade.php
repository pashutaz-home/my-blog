@extends('layouts.main')


@section('content')
    <div id="wrapper3">
        <div id="portfolio" class="container">
            <div class="title">
                <h2>Design Portfolio</h2>
                <span class="byline">Integer sit amet pede vel arcu aliquet pretium</span> </div>
            <div class="pbox1">
                <div class="column1">
                    <div class="box"> <span class="icon icon-comments"></span>
                        <h3>Vestibulum venenatis</h3>
                        <p>Fermentum nibh augue praesent a lacus at urna congue rutrum.</p>
                    </div>
                </div>
                <div class="column2">
                    <div class="box"> <span class="icon icon-cogs"></span>
                        <h3>Praesent scelerisque</h3>
                        <p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
                    </div>
                </div>
                <div class="column3">
                    <div class="box"> <span class="icon icon-coffee"></span>
                        <h3>Donec dictum metus</h3>
                        <p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
                    </div>
                </div>
                <div class="column4">
                    <div class="box"> <span class="icon icon-cloud"></span>
                        <h3>Mauris vulputate dolor</h3>
                        <p>Rutrum fermentum nibh in augue praesent urna congue rutrum.</p>
                    </div>
                </div>
            </div>
            <div class="pbox2">
                <div class="column1">
                    <div class="box"> <span class="icon icon-asterisk"></span>
                        <h3>Rhoncus volutpat</h3>
                        <p>Fermentum nibh augue praesent a lacus at urna congue rutrum.</p>
                    </div>
                </div>
                <div class="column2">
                    <div class="box"> <span class="icon icon-headphones"></span>
                        <h3>Sed odio sagittis</h3>
                        <p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
                    </div>
                </div>
                <div class="column3">
                    <div class="box"> <span class="icon icon-user"></span>
                        <h3>Aenean elementum</h3>
                        <p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
                    </div>
                </div>
                <div class="column4">
                    <div class="box"> <span class="icon icon-signal"></span>
                        <h3>Etiam posuere augue</h3>
                        <p>Rutrum fermentum nibh in augue praesent urna congue rutrum.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection