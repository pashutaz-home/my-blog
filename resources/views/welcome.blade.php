@extends('layouts.main')

@section('header')
    <div id="header-wrapper">
        <div id="header" class="container">
            <div id="logo">
                <h1><a href="/welcome">Pashutaz Portfolio</a></h1>
                <span>Made by <a href="https://github.com/pashutaz/" rel="nofollow" target="_blank">Pashutaz</a></span>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div id="wrapper1">
        <div id="welcome" class="container">
            <div class="title">
                <h2>Welcome to my website</h2>
                <span class="byline">Mauris vulputate dolor sit amet nibh</span>
            </div>
        </div>
    </div>
@endsection

