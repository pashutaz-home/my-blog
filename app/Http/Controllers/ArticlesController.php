<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Article;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
	public function show(Article $article)
	{
		if ($article) {
			return view('articles.show', ['article' => $article]);
		} else {
			abort(404);
		}
	}

	public function index()
	{
		if (request('tag')) {
			$articles = Tag::where('name', request('tag'))->firstOrFail()->articles;
		} else {
			$articles = Article::latest()->get();
		}
		return view('articles.index', [
			'articles' => $articles
		]);
	}

	public function create()
	{
		return view('articles.create', [
			'tags' => Tag::all()
		]);
	}

	public function store()
	{
		$article = new Article($this->validateArticle());
		$article->user_id = 3;
		$article->save();

		$article->tags()->attach(request('tags'));

		return redirect('/articles');
	}

	public function edit(Article $article)
	{
		return view('articles.edit', [
			'article' => $article,
			'tags' => Tag::all()
		]);
	}

	public function update(Article $article)
	{
		$article->update($this->validateArticle());

		$article->tags()->attach(request('tags'));

		return redirect($article->path());
	}

	public function delete(Article $article)
	{
		$article->delete();

		return redirect('/articles');
	}

	public function validateArticle() {
		return request()->validate([
			'title' => ['required', 'min:5', 'max:255'],
			'summary' => 'nullable',
			'body' => 'required',
			'tags' => 'exists:tags,id'
		]);
	}
}
